from django.http import HttpResponse
from django.conf import settings


class IPRestrictionMiddleware(object):
    def process_request(self, request):
        setting = getattr(settings, 'IP_RESTRICTION', None)
        if setting:
            if type(setting) == dict:
                ips = setting.get('IPS', ())
                if not type(ips) == tuple:
                    return HttpResponse('Restricted IPS list must be a tuple!', status=500)

                status_code = setting.get('STATUS_CODE', 403)
                if not type(status_code) == int:
                    return HttpResponse('STATUS_CODE must be an integer!', status=500)

                paths = setting.get('PATHS', ('/admin',))
                if not type(paths) == tuple:
                    return HttpResponse('PATHS must be a tuple!', status=500)

                ip = request.META.get('HTTP_X_FORWARDED_FOR', request.META['REMOTE_ADDR'])
                if ips and ip not in ips:
                    request.META['IS_RESTRICTED_IP'] = False
                    for path in paths:
                        if request.META['PATH_INFO'].startswith(path):
                            return HttpResponse(status=status_code)
                else:
                    request.META['IS_RESTRICTED_IP'] = True
            else:
                return HttpResponse('IP_RESTRICTION setting must be a dict!', status=500)

