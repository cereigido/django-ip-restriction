================
Requirements
================

- `Python <http://www.python.org/>`_ 2.6+
- `Django <http://www.djangoproject.com>`_ 1.3+

================
Installation
================

To install django-ip-restriction is easy::

    pip install https://cereigido@bitbucket.org/cereigido/django-ip-restriction.git

================
Setup
================

Some changes to your Django project's settings.py must be done

Adding the middleware
=====================

The middleware class must be added to your settings.py as show bellow::

    MIDDLEWARE_CLASSES = (
        ...
        'iprestriction.middleware.IPRestrictMiddleware',
        ...
    )

Configuring IP_RESTRICION dict
==============================

To restrict IP access to the desired paths you must also add a dict named IP_RESTRICTION to settings.py.

+-------------+-----------------------------------------------------------------------------+-------+-------------+
| Key         | Description                                                                 | Type  | Default     |
+=============+=============================================================================+=======+=============+
| IPS         | A tuple containing the allowed IP addresses to access the restricted paths. | tuple | ()          |
+-------------+-----------------------------------------------------------------------------+-------+-------------+
| PATHS       | A tuple containing the paths to restrict access.                            | tuple | ('/admin',) |
+-------------+-----------------------------------------------------------------------------+-------+-------------+
| STATUS_CODE | The HTTP Response code to be returned in case of unauthorized access.       | int   | 403         |
+-------------+-----------------------------------------------------------------------------+-------+-------------+

Sample::

    IP_RESTRICTION = {
        'IPS': ('192.168.0.99',),
        'PATHS': ('/admin', '/reports',),
        'STATUS_CODE': 404,
    }

==============
Context vars
==============

A context var called IS_RESTRICTED_IP is added to your request's META allowing you to identify if the current user's IP is allowed to access you restricted path(s).

