#!/usr/bin/env python

from iprestriction import __version__
from distutils.core import setup
from setuptools import find_packages

setup(
    name='django-ip-restriction',
    version=__version__,
    url='https://bitbucket.org/cereigido/django-ip-restriction',
    author='Paulo Cereigido',
    author_email='paulocereigido@gmail.com',
    description='Django middleware that lets you configure restricted paths from your application to be acessed only by a set of IPs.',
    packages=find_packages(),
    include_package_data=True,
    requires=[
        'django (>= 1.3)',
    ],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Python Software Foundation License',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Topic :: Software Development',
    ],
)
